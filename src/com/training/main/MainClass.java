package com.training.main;

import com.training.operations.Operations;

public class MainClass {

	public static void main(String[] args) {
		Operations operations = new Operations();
		operations.add();
		operations.sub();
		operations.mul();
		operations.div();
		operations.mod();
	}
}