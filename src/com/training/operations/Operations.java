package com.training.operations;

public class Operations {

	int a = 100;
	int b = 40;

	public void add() {
		System.out.println("Addition : " + (a + b));
	}

	public void sub() {
		System.out.println("Subtraction : " + (a - b));
	}

	public void mul() {
		System.out.println("Multiplication : " + (a * b));
	}

	public void div() {
		System.out.println("Division : " + (a / b));
	}

	public void mod() {
		System.out.println("Modulo Division : " + (a % b));
	}
}